/**
 * Write a program that outputs the average of `4`, `5`, and `6`, using a function
 * `average(a, b, c)` that accepts three parameters and returns the average.
 * It should not calculate the sum by itself, but should rather use the sum3 function you previously wrote.
 *
 * Don't forget to test the function!
 */

function sum (a, b, c) {
  // implement this function!
  return a+b+c;
}

function average(a,b,c){
    return sum(a,b,c)/3;
}
console.log(average(4, 5, 6))
