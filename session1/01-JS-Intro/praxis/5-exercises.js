/**
 * Implement the functions below. Don't forget to test them, or to use the tests
 * that are given in the code!
 */

/**
 * Write a function that checks whether a number is even or not.
 *
 * Hint: How does one know if a number is even? Because dividing by zero has a zero remainder.
 * And how do you find the remainder? Using the `%` operator.
 *
 * Hint: It does not _print_ the boolean, but rather _returns_ it.
 *
 * isEven(5) ==> false
 * isEven(896) ==> true
 */
function isEven (num) {
  if(num%2===0){
    return true;
  }else{
    return false;
  }
}

// Tests:
if (isEven(5) !== false) {
  throw 'failed'
}
if (isEven(896) !== true) {
  throw 'failed'
}

/**
 * Write a function that checks whether a number is odd or not.
 *
 * Hint: Use isEven.
 * 
 * 
// Tests:
// write the tests...
 * isOdd(5) ==> true
 * isOdd(896) ==> false
 */
function isOdd(num){
  return !isEven(num);
}

if (!isOdd(5)) {
  throw 'failed'
}
if (isOdd(896)) {
  throw 'failed'
}


/**
 * Write a program that receives the three grades the student got.
 * If the average > 60, return true, otherwise return false.
 *
 * didStudentPass(60, 70, 80) ==> true
 * didStudentPass(40, 50, 60) ==> false
 */
function didStudentPass (gradeA, gradeB, gradeC) {
  var avg = (gradeA + gradeB + gradeC)/3
  if(avg >60){
    return true;
  }
  else{
    return false;
  }
}

// Tests:
if (didStudentPass(60, 70, 80) !== true) {
  throw 'failed'
}
if (didStudentPass(40, 50, 60) !== false) {
  throw 'failed'
}

/**
 * returns true if n is inside the range rangeStart.. rangeEnd (including rangeStart and rangeEnd)
 *
 * Hint: 5 is inside the range 4..9 because 5 is bigger than 4 and 5 is smaller than 9.
 *
 * insideOf(5, 4, 9) ==> true
 * insideOf(9, 4, 9) ==> true
 * insideOf(10, 4, 9) ==> false
 * insideOf(3, 4, 9) ==> false
 *
 */
function insideOf (n, rangeStart, rangeEnd) {
  if(n >= rangeStart && n <= rangeEnd){
    return true;
  }else{
    return false;
  }
}

// Tests:
if (insideOf(5, 4, 9) !== true) {
  throw 'failed'
}
if (insideOf(9, 4, 9) !== true) {
  throw 'failed'
}
if (insideOf(10, 4, 9) !== false) {
  throw 'failed'
}
if (insideOf(3, 4, 9) !== false) {
  throw 'failed'
}

/**
 * returns true if n is outside the range rangeStart.. rangeEnd
 *
 * outsideOf(5, 4, 9) ==> false
 * outsideOf(9, 4, 9) ==> false
 * outsideOf(10, 4, 9) ==> true
 * outsideOf(3, 4, 9) ==> true
 *
 */
function outsideOf (n, rangeStart, rangeEnd) {

}

// Tests:
// write the tests...

/**
 * Write a function that translates the words "hello", "goodbye"
 * in two languages ("french", "spanish") to english.
 * If it receives a word it does not know, or a language it does not now, it returns "sorry".
 */
function translate (word, language) {
      if(language === 'french'){
          if(word === 'bonjour'){
            return 'hello';
          }else if(word === 'au revoir'){
            return 'goodbye';
          }
      }else if(language === 'spanish'){
          if(word === 'Hola'){
            return 'hello';
          }else if(word === 'adios'){
            return 'goodbye';
          }
      }
      return 'sorry';
}

// Tests:
if (translate('bonjour', 'french') !== 'hello') {
  throw 'failed'
}
if (translate('au revoir', 'french') !== 'goodbye') {
  throw 'failed'
}
if (translate('Hola', 'spanish') !== 'hello') {
  throw 'failed'
}
if (translate('adios', 'spanish') !== 'goodbye') {
  throw 'failed'
}
if (translate('ooolloooo', 'french') !== 'sorry') {
  throw 'failed'
}
if (translate('boooloooo', 'spanish') !== 'sorry') {
  throw 'failed'
}
if (translate('boooloooo', 'burmesian') !== 'sorry') {
  throw 'failed'
}

/**
 * Write a function that returns "morning", "noon", "afternoon", "evening", or "night",
 * depending on hours and minutes.
 * morning - 6:00 - 11:29
 * noon - 11:30 - 13:00
 * afternoon - 13:01 - 17:00
 * evening - 17:01 - 20:30
 * night - 20:31 - 5:59
 *
 * Hint: it's morning if the hour is between 6 and 10. The hour 11 is special because if the hour is 11
 * then the minutes have to be smaller than 30. Do the same for the rest of the ranges.
 */
function timeOfDay (hour, minute) {
  if(hour >= 6 && hour < 11){
    return 'morning';
  }else if(hour === 11 && minute >= 0 && minute <= 29){
    return 'morning';
  }else if(hour === 11 && minute >= 30 && minute <= 59){
    return 'noon';
  }else if(hour === 12){
    return 'noon';
  }else if(hour === 13 && minute === 0){
    return 'noon';
  }else if(hour === 13 && minute >= 1 && minute <= 59){
    return 'afternoon';
  }else if(hour === 14 || hour === 15 || hour === 16){
    return 'afternoon';
  }else if(hour === 17 && minute === 0){
    return 'afternoon';
  }else if(hour === 17 && minute >= 1 && minute <= 59){
    return 'evening';
  }else if(hour === 18 || hour === 19){
    return 'evening';
  }else if(hour === 20 && minute <= 30 && minute >= 0){
    return 'evening';
  }else if(hour === 20 && minute >= 31 && minute <= 59){
    return 'night';
  }else if(hour >= 21 && hour < 24){
    return 'night';         
  }else if(hour >= 0 && hour <= 5){
    return 'night';
  }
}

// Tests:
if (timeOfDay(6, 0) !== 'morning') {
  throw 'failed'
}
if (timeOfDay(10, 23) !== 'morning') {
  throw 'failed'
}
if (timeOfDay(11, 10) !== 'morning') {
  throw 'failed'
}
if (timeOfDay(11, 30) !== 'noon') {
  throw 'failed'
}
if (timeOfDay(12, 30) !== 'noon') {
  throw 'failed'
}
if (timeOfDay(13, 0) !== 'noon') {
  throw 'failed'
}
if (timeOfDay(13, 1) !== 'afternoon') {
  throw 'failed'
}
if (timeOfDay(16, 30) !== 'afternoon') {
  throw 'failed'
}
if (timeOfDay(17, 0) !== 'afternoon') {
  throw 'failed'
}
if (timeOfDay(17, 1) !== 'evening') {
  throw 'failed'
}
if (timeOfDay(19, 30) !== 'evening') {
  throw 'failed'
}
if (timeOfDay(20, 0) !== 'evening') {
  throw 'failed'
}
if (timeOfDay(20, 30) !== 'evening') {
  throw 'failed'
}
if (timeOfDay(20, 31) !== 'night') {
  throw 'failed'
}
if (timeOfDay(23, 59) !== 'night') {
  throw 'failed'
}
if (timeOfDay(0, 0) !== 'night') {
  throw 'failed'
}
if (timeOfDay(0, 30) !== 'night') {
  throw 'failed'
}
if (timeOfDay(5, 59) !== 'night') {
  throw 'failed'
}

/**
 * Bonus:
 *
 * Reverses a one or two digit number.
 * reverseTwoDigitNumber(3) ==> 30 (because 3 "===" 03)
 * reverseTwoDigitNumber(34) ==> 43
 * reverseTwoDigitNumber(0) => 0
 *
 * You can use Math.floor(). Math.floor(4) ==> 4, Math.floor(4.7) ==> 4, Math.floor(4.2) ==> 4
 *
 * How? Let's take 34. Extract the 4 using 34 % 10. Now extract the 3 using 34 / 10.
 * Now that you have the 3 and 4, rebuild the number using 4 * 10 + 3.
 *
 * If you do it this way, then 3 => 30, and 0 => 0
 * will just happen without needing any special programming.
 *
 */
function reverseTwoDigitNumber (n) {
  var str = n.toString();
  if(str.length === 1){
      str+=0;
      return parseInt(str);
  }else if(str.length === 2){
      var splitStr = str.split("");
      var reverseArr = splitStr.reverse();
      var joinArr = reverseArr.join("");
      return parseInt(joinArr);
  }
}

// Tests:
if (reverseTwoDigitNumber(3) !== 30) {
  throw 'failed'
}
if (reverseTwoDigitNumber(34) !== 43) {
  throw 'failed'
}
if (reverseTwoDigitNumber(0) !== 0) {
  throw 'failed'
}

/**
 * Bonus:
 *
 * Reverses a one or two digit or three digit number. You can (and should) use
 * 'reverseTwoDigitNumber' in your implementation
 * reverseThreeDigitNumber(3) ==> 300 (because 3 "===" 003)
 * reverseThreeDigitNumber(34) ==> 430
 * reverseThreeDigitNumber(345) ==> 543
 * reverseThreeDigitNumber(0) => 0
 *
 */
function reverseThreeDigitNumber(n) {
  var str = n.toString();
    if(str.length === 1){
        str+=0;
        return parseInt(str);
    }else if(str.length === 2 || str.length === 3){
        var splitStr = str.split("");
        var reverseArr = splitStr.reverse();
        var joinArr = reverseArr.join("");
      return parseInt(joinArr);
    }
}

// Tests:
if (reverseThreeDigitNumber(3) !== 30) {
  throw 'failed'
}
if (reverseThreeDigitNumber(34) !== 43) {
  throw 'failed'
}
if (reverseThreeDigitNumber(345) !== 543) {
  throw 'failed'
}
if (reverseThreeDigitNumber(0) !== 0) {
  throw 'failed'
}