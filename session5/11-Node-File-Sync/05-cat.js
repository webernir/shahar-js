const fs = require('fs')

const filesToRead = process.argv.slice(2)

console.log('filesToRead',filesToRead)

for (const fileToRead of filesToRead) {
  const text = fs.readFileSync(fileToRead, { encoding: 'utf-8' })

  console.log(text)
}

/**
 * This is 'cat' from the CLI. An exact implementation.
 *
 * To run it, you need a terminal, because of the arguments
 */
// node 05-cat.js workfiles/1.txt workfiles/hello.txt workfiles/hello.txt