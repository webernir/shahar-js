function map(arr, func) {
  const ret = []

  for (const i in arr) {
    ret.push(func(arr[i]))
  }

  return ret
}

console.log(map([2, 5, 3], (n) => n + 1)) // [ 3, 6, 4 ]
console.log(map([2, 5, 3], (n) => n - 1)) // [ 1, 4, 2 ]
console.log(map([2, 5, 3], n => n * n))   // [ 4, 25, 9 ]

/**
 * Arrow functions
 * Parameter can be with or without ()
 */

// More Examples:
const a = () => 'nir'
const b = name => `hello ${name}`
const c = (name) => `hello ${name}`
const d = (name, age) => `hello ${name} at ${age}`
const e = (name, age) => {
  console.log('user message...')
  return `hello ${name} at ${age}`
}
const f = () => console.log('yo yo yo')
const g = () => { console.log('yo yo yo') }
const h = () => { return { name: 'nir', age: 33 } }
const h = () => ({ name: 'nir', age: 33 })