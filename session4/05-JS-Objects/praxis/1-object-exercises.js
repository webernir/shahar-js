/**
 * Implement the following functions
 */

/**
 * Returns the name of the city with the highest population.
 * A city has the fields `population` and `name`
 *
 * maxPopulation({name: 'Haifa', population: 200000}, {name: 'Tel Aviv', population: 500000}) ==> 'Tel Aviv'
 */
function maxPopulation2 (city1, city2) {
  return city1.population > city2.population ? city1.name : city2.name
}

const haifa = { name: 'Haifa', population: 200000 }
const telAviv = { name: 'Tel Aviv', population: 500000 }
const jerusalem = { name: 'Jerusalem', population: 750000 }

// Tests:
if (maxPopulation2(haifa, telAviv) !== 'Tel Aviv') {
  throw 'failed'
}

if (maxPopulation2(jerusalem, telAviv) !== 'Jerusalem') {
  throw 'failed'
}

/**
 * Returns the name of the city with the highest population from an array of cities.
 * A city has the fields `population` and `name`
 *
 * maxPopulation([{name: 'Haifa', population: 200000}, {name: 'Tel Aviv', population: 500000}]) ==> 'Tel Aviv'
 */
function maxPopulation (cities) {
  let maxPopCity = cities[0]
  for(let i = 1; i < cities.length; i++){
    if(cities[i].population > maxPopCity.population)
     maxPopCity = cities[i]   
  }
  return maxPopCity.name
}

// Tests:
if (maxPopulation([haifa, telAviv, jerusalem]) !== 'Jerusalem') {
  throw 'failed'
}
if (maxPopulation([haifa, jerusalem, telAviv]) !== 'Jerusalem') {
  throw 'failed'
}
if (maxPopulation([telAviv, haifa]) !== 'Tel Aviv') {
  throw 'failed'
}
if (maxPopulation([haifa]) !== 'Haifa') {
  throw 'failed'
}

/**
 * Returns a string with instructions on whether the players can play game of thrones
 * Remember, only adults (>= 18( can play.
 * A player has the fields `age` and `name`.
 *
 * canPlayGameOfThrones({age: 18, name: 'Hillel'}, {age: 19, name: 'Dima'}) ==> 'Hillel and Dima can play!'
 * canPlayGameOfThrones({age: 17, name: 'Hillel'}, {age: 19, name: 'Dima'}) ==> "Hillel and Dima can't play, because Hillel is too young!"
 * canPlayGameOfThrones({age: 19, name: 'Hillel'}, {age: 17, name: 'Dima'}) ==> "Hillel and Dima can't play, because Dima is too young!"
 * canPlayGameOfThrones({age: 17, name: 'Hillel'}, {age: 17, name: 'Dima'}) ==> "Hillel and Dima can't play, because they're both too young!"
 */
function canPlayGameOfThrones (player1, player2) {
  let retStr
  if(player1.age >= 18 && player2.age >= 18){
    retStr = `${player1.name} and ${player2.name} can play!`
  }else if(player1.age < 18 && player2.age < 18){
    retStr = `${player1.name} and ${player2.name} can't play, because they're both too young!`
  }else{
    const young = player1.age < player2.age ? player1 : player2
    retStr = `${player1.name} and ${player2.name} can't play, because ${young.name} is too young!`
  }
  return retStr
}

// Tests:
 if(canPlayGameOfThrones({age: 18, name: 'Hillel'}, {age: 19, name: 'Dima'}) !== 'Hillel and Dima can play!'){
   throw 'failed'
 }
 if(canPlayGameOfThrones({age: 17, name: 'Hillel'}, {age: 19, name: 'Dima'}) !== "Hillel and Dima can't play, because Hillel is too young!"){
   throw 'faied'
 }
 if(canPlayGameOfThrones({age: 19, name: 'Hillel'}, {age: 17, name: 'Dima'}) !== "Hillel and Dima can't play, because Dima is too young!"){
   throw 'failed'
 }
 if(canPlayGameOfThrones({age: 17, name: 'Hillel'}, {age: 17, name: 'Dima'}) !== "Hillel and Dima can't play, because they're both too young!"){
   throw 'failed'
 }


/**
 * write a function that accepts an array of arrays, and turns them into objects, where
 * each inner array is a pair of key/values.
 *
 * objectFromPairs([['name', 'Donald Trump'], ['age', 70]]) ==> {name: 'Donald Trump', age: 70}
 */
function objectFromPairs (fieldPairs) {
  const data = {}
  fieldPairs.map((obj) => {
    data[obj[0]] = obj[1]
  })
  return data
}

// Tests:
const o1 = objectFromPairs([['name', 'Donald Trump'], ['age', 70]])
if (o1.name !== 'Donald Trump') {
  throw 'failed'
}
if (o1.age !== 70) {
  throw 'failed'
}

/**
 * returns whether two objects are the same.
 *
 * isObjectEqual({}, {}) ==> true
 * isObjectEqual({name: 'Donald', age: 70}, {age: 70, name: 'Donald'}) ==> true
 * isObjectEqual({name: 'Donald', age: 70, wife: 'Melania'}, {age: 70, name: 'Donald'}) ==> false
 *
 * Use Object.keys(o) to get an array of key names from an object,
 * and then iterate on them in tandem using a:
 * for (i in Object.keys(o1)) {
 * }
 *
 * In the body of the loop you can check for equality of the values in o1 and o2.
 */
function isObjectEqual (o1, o2) {
  const keys = Object.keys(o1)
  let ret = false 
  if(Object.keys(o1).length === Object.keys(o2).length){
    for(i in keys){
      if(o1[keys[i]] !== o2[keys[i]]){
        return ret
      }
    }
    ret = true
  }
  return ret
}

// Tests:
if(!isObjectEqual({}, {})){
  throw 'failed'
}
if(!isObjectEqual({name: 'Donald', age: 70}, {age: 70, name: 'Donald'})){
  throw 'failed'
}
if(isObjectEqual({name: 'Donald', age: 70, wife: 'Melania'}, {age: 70, name: 'Donald'})){
  throw 'failed'
}

/**
 * Bonus:
 *
 * Returns an object who's keys are the array elements, and whose value is the
 * number of times it appears in the array
 *
 * frequency(['a', 'b', 'a', 'b', 'a', 'a', 'c']) ==> {'a': 4, 'b': 2, 'c': 1}
 * frequency([]) ==> {}
 */
function frequency (arr) {
  const ret = arr.reduce((tally,addition) => {
    tally[addition] = (tally[addition] || 0) + 1
    return tally
  }, {})
  return ret
}

// Tests:
if(!isObjectEqual(frequency(['a', 'b', 'a', 'b', 'a', 'a', 'c']), {'a': 4, 'b': 2, 'c': 1})){
  throw 'failed'
}
if(!isObjectEqual(frequency([]), {})){
  throw 'failed'
}
