/**
 * Implement the functions below. Don't forget to test them.
 */
/**
 * Write a function that returns the typeof of the entity.
 * In the case of an array, it will return 'array'.
 */

function myTypeof (entity) {
   return Array.isArray(entity) ? 'array' : typeof(entity)
}

if(myTypeof([1,2,3]) !== 'array'){
    throw 'failed'
}
if(myTypeof(1) !== 'number'){
    throw 'failed'
}

/**
 * Write a function that accepts 1, 2, 3, or 4 numbers and calculates the average
 * (depending on how many were passed).
 * How do you know how many were passed? If 3 were passed, then d will be undefined, etc.
 */
function average4 (a, b, c, d) {
    const args = [...arguments]
    const passedArgs = args.filter((a) => typeof(a) !== undefined)
    return passedArgs.reduce((sum,current) => sum += a, 0)/passedArgs.length
}

if(average4 (3,5,3,1) !== 3){
    throw 'failed'
}
if(average4(2,2,5) !== 3){
    throw 'failed'
}

/**
 *
 */
