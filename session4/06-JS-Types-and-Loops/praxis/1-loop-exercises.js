/**
 * Implement the functions below. Don't forget to test them!
 */

/**
 * Reverses a number, no matter how many digits it has.
 *
 * reverseNumber(3) ==> 3
 * reverseNumber(34) ==> 43
 * reverseNumber(73463) => 36437
 *
 * How? To extract the rightmost digit, do n % 10. To "remove" it from the number, do Math.floor(n / 10).
 * To "push" a digit d (assuming r is 0), do r * 10 + d
 *
 * Now loop over the number n, extract digits, and push them to a result. Do that till... well, you figure it out
 */
function reverseNumber (n) {
    let reverse = 0
    while(n > 0){
        reverse = reverse * 10 + n % 10
        n = Math.floor(n / 10)
    }
    return reverse
}

// Tests
if(reverseNumber(3) !== 3){
    throw 'failed'
}
if(reverseNumber(34) !== 43){
    throw 'failed'
}
if(reverseNumber(73463) !== 36437){
    throw 'failed'
}

/**
 * Write a function that returns an array with the range startRange, endRange
 * (including startRange, not including endRange).
 *
 * You are not allowed to use for. Hint: but you _can_ use while...
 */
function printRange (startRange, endRange) {
    const arr = [startRange]
    while(startRange < endRange-1){
		startRange += 1
        arr.push(startRange)
    }
    return arr
}

// Tests
if(printRange(30,41) !== [30,31,32,33,34,35,36,37,38,39,40]){
    throw 'failed'
}
if(printRange(0,1) !== []){
 throw 'failed'
}

/**
 * Write a function that returns an array with the range startRange, endRange
 * (including startRange, and *yes* including endRange).
 *
 * You are not allowed to use a for loop, just while.
 */
function printRangeInclusive (startRange, endRange) {
    const arr = [startRange]
    while(startRange <= endRange-1){
		startRange += 1
        arr.push(startRange)
    }
    return arr
}
