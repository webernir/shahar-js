function arrayMax (arr) {
  let maxTillNow = arr[0]

  for (const v of arr) {
    if (v > maxTillNow) {
      maxTillNow = v
    }
  }

  return maxTillNow
}

console.log(arrayMax([]))
